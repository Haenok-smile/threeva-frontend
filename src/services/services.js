import Api from './api'

export default {
  getDoctors (params) {
    return Api().post('/search_doctor', params)
  },
  getCities () {
    return Api().get('/get_city')
  },
  getAreas (params) { 
    return Api().post('/get_region_by_city', params)
  },
  getSpecialities (params) {
    return Api().post('/get_speciality', params)
  }
}