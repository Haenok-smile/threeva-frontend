export default {
  setSearchData,
  setCityData,
  setAreaData,
  setSpecialityData,
  setDoctors
}

function setSearchData (state, payload) {
  state.searchData = payload
}
function setCityData (state, payload) {
  state.cityData = payload
}
function setAreaData (state, payload) {
  state.areaData = payload
}
function setSpecialityData (state, payload) {
  state.specialityData = payload
}
function setDoctors (state, payload) {
  state.doctors = payload
}