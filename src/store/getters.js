export const getSearchData = (state) => {
  return state.searchData
}
export const getCityData = (state) => {
  return state.cityData
}
export const getAreaData = (state) => {
  return state.areaData
}
export const getSpecialityData = (state) => {
  return state.specialityData
}
export const getDoctors = (state) => {
  return state.doctors
}
export const getSelectedCity = (state) => {
  return state.selectedCity
}